
# Setting up a python enviroment in Ubuntu 22.04

### 1. Install build-essential, gsl and other making stuff.

```{r, engine='bash', count_lines}
sudo apt install build-essential libgsl-dev libgslcblas0 pkg-config cmake cmake-qt-gui ninja-build valgrind htop git 
```

### 2. Configure git.
```{r, engine='bash', count_lines}
git config --global user.name "YOUR_NAME"
git config --global user.email YOUR_EMAIL
git config --global credential.helper store
```

### 3.  Install python natively via apt.

```{r, engine='bash', count_lines}
sudo apt install python3 python3-wheel python3-pip python3-venv python3-dev python3-setuptools
```

### 4. Install numpy, scipy, and other basic modules via pip.

If you have conda installed, first desactivate it.

```{r, engine='bash', count_lines}
conda config --set auto_activate_base false
conda desactivate
```

```{r, engine='bash', count_lines}
sudo pip install --upgrade numpy scipy matplotlib ipython jupyter pandas sympy nose openpyxl
sudo pip install --upgrade scikit-learn scikit-image
```

### 5.  Install nbextensions.

```{r, engine='bash', count_lines}
sudo pip install jupyter_contrib_nbextensions
jupyter contrib nbextension install --user
```

### 6. Install networkx and igraph.

```{r, engine='bash', count_lines}
sudo pip install networkx igraph
```

### 7.  Install graph-tool.

```{r, engine='bash', count_lines}
sudo add-apt-repository "deb [ arch=amd64 ] https://downloads.skewed.de/apt jammy main"
#apt-key adv --keyserver keys.openpgp.org --recv-key 612DEFB798507F25
wget -O- https://keys.openpgp.org/vks/v1/by-fingerprint/793CEFE14DBC851A2BFB1222612DEFB798507F25 | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/graph-tool-archive.gpg
sudo apt update
sudo apt install python3-graph-tool
```

### 8.  Install pyhvr.

```{r, engine='bash', count_lines}
mkdir ~/pypack
cd ~/pypack
git clone https://gitlab.com/hvribeiro/pyhvr.git
cd pyhvr
sudo pip install .
```

### 9.  Install chvr.
```{r, engine='bash', count_lines}
mkdir ~/pypack
cd ~/pypack
git clone https://github.com/hvribeiro/chvr.git
cd chvr/sources
mkdir bin
make
cd ../chvr/
sudo pip install .
```

### 10. Enable Helvetica in matplolib. 
```{r, engine='bash', count_lines}
cd ~/pypack/pyhvr/
sudo tar -xvf helv.tar.gz -C /usr/local/lib/python3.10/dist-packages/matplotlib/mpl-data/fonts/ttf/
mkdir ~/.matplotlib
cp /usr/local/lib/python3.10/dist-packages/matplotlib/mpl-data/matplotlibrc ~/.matplotlib/.
```

Edit `~/.matplotlib/matplotlibrc` and modify the line starting with `'#font.sans-serif: DejaVu Serif, [...]'` to include Helvetica in the first position.

```{r, engine='bash', count_lines}
font.sans-serif: Helvetica, DejaVu Sans, [...]
```

Add `~/.matplotlib` to to PATH by adding the folowing line at the end of the file `~/.bashrc`.
```{r, engine='bash', count_lines}
export MPLCONFIGDIR=$HOME/.matplotlib
```
You may need to verify the matplotlib and modify the path (`/usr/local/lib/python3.10/dist-packages`) accordingly. 

```{r, engine='bash', count_lines}
python3 -c "import matplotlib; print(matplotlib.matplotlib_fname())"
```

You may also need to remove font cache files
```{r, engine='bash', count_lines}
rm ~/.matplotlib/fontList.cache ~/.matplotlib/fontManager.cache ~/.matplotlib/ttffont.cache
```

### 11. Install dask and p_tqdm.

```{r, engine='bash', count_lines}
sudo python3 -m pip install "dask[complete]"
sudo pip install p_tqdm
```

### 12. Install beautifulsoup4, scrapy, and selenium.

```{r, engine='bash', count_lines}
sudo pip install beautifulsoup4 scrapy selenium
```

### 13. Install miniconda in case you need conda for particular packages.

```{r, engine='bash', count_lines}
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh
source ~/miniconda3/bin/activate 
conda config --set auto_activate_base false
conda deactivate
 ```

### 14.  Install the nvidia driver version 510 (nvidia-driver-510), a version compatible with nvidia-cuda-toolkit and cudnn shipped with Ubuntu 22.04 (`11.5.1-1ubuntu1` and `8.2.4.15~cuda11.4`). Required for using GPU-related packages.

```{r, engine='bash', count_lines}
sudo apt install nvidia-driver-510
sudo reboot
```

### 15. Install cuda-toolkit and cudnn.

```{r, engine='bash', count_lines}
sudo apt install nvidia-cuda-toolkit nvidia-cudnn
```

 ### 16.  Install tensorflow and torch with gpu support via pip (for cuda 11.3).

```{r, engine='bash', count_lines}
sudo pip install --upgrade tensorflow-gpu tensorboard keras
sudo pip install --upgrade torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu113
```

Test the instalation.
```{r, engine='bash', count_lines}
python3 -c "from tensorflow.python.client import device_lib; print(device_lib.list_local_devices())"
python3 -c "import torch; print(torch.cuda.is_available())"
```

### 17. Install torch-geometric (for cuda 11.3).


```{r, engine='bash', count_lines}
sudo pip install torch-scatter -f https://data.pyg.org/whl/torch-1.12.0+cu113.html
sudo pip install torch-sparse -f https://data.pyg.org/whl/torch-1.12.0+cu113.html
sudo pip install torch-cluster -f https://data.pyg.org/whl/torch-1.12.0+cu113.html

```

Next, install torch-geometric.

```{r, engine='bash', count_lines}
sudo pip install torch-geometric
```

### 18. Install DGL (for cuda 11.3).

```{r, engine='bash', count_lines}
sudo pip install dgl-cu113 dglgo -f https://data.dgl.ai/wheels/repo.html
```

### 19. Install umap.

```{r, engine='bash', count_lines}
sudo pip install umap-learn
```

### 20. Install pymc with gpu support (jax).

```{r, engine='bash', count_lines}
sudo pip install "jax[cuda11_cudnn82]" -f https://storage.googleapis.com/jax-releases/jax_cuda_releases.html
sudo pip install pymc
sudo pip install numpyro
```