# Setting up a Python Environment in Ubuntu 24.04

### 1. Install essential build tools, GSL, and other necessary packages.

```{r, engine='bash', count_lines}
sudo apt install build-essential libgsl-dev libomp-dev libgslcblas0 pkg-config cmake cmake-qt-gui ninja-build valgrind htop git 
```

### 2. Configure Git.
```{r, engine='bash', count_lines}
git config --global user.name "YOUR_NAME"
git config --global user.email YOUR_EMAIL
git config --global credential.helper store
```

### 3. Install `graph-tool`.

```{r, engine='bash', count_lines}
sudo apt install python3-graph-tool
```

### 4. Install Python and related tools via `apt`.

```{r, engine='bash', count_lines}
sudo apt install python3 python3-wheel python3-pip python3-venv python3-dev python3-setuptools
```

### 5. Create and activate a virtual environment..

```{r, engine='bash', count_lines}
python3 -m venv clab
source clab/bin/activate
```

### 6. Make this virtual environment load automatically.

Open `~/.bashrc` file 

```{r, engine='bash', count_lines}
nano ~/.bashrc
```

Add the following line at the end of the file:

```{r, engine='bash', count_lines}
source ~/clab/bin/activate
```

To make graph-tool and other system modules available in the environment, create a file
```{r, engine='bash', count_lines}
nano ~/clab/lib/python3.12/site-packages/dist-packages.pth
```
containing `/usr/lib/python3/dist-packages`. Note that the path may vary.

### 7. Install basic modules using `pip`.

```{r, engine='bash', count_lines}
pip install --upgrade numpy scipy matplotlib ipython jupyter pandas sympy statsmodels nose openpyxl scikit-learn xgboost catboost scikit-image networkx igraph
```

Install `ordpy` and `knnpe`
```{r, engine='bash', count_lines}
pip install ordpy
pip install git+https://github.com/hvribeiro/knnpe
```


### 8. Install `dask` and `p_tqdm`.

```{r, engine='bash', count_lines}
python3 -m pip install "dask[complete]"
pip install p_tqdm
```

### 9. Install `beautifulsoup4`, `scrapy`, and `selenium`.

```{r, engine='bash', count_lines}
pip install beautifulsoup4 scrapy selenium
```

### 10. Install Jupyter Lab extensions.

```{r, engine='bash', count_lines}
pip installjupyterlab-lsp
pip install 'python-lsp-server[all]'
pip install jupyterlab_execute_time
sudo apt-get install ripgrep
pip install jupyterlab-search-replace
```


### 11. Install `pyhvr` and `chvr`.

```{r, engine='bash', count_lines}
mkdir ~/pypack
cd ~/pypack
git clone https://gitlab.com/hvribeiro/pyhvr.git
cd pyhvr
pip install .
```

```{r, engine='bash', count_lines}
cd ~/pypack
git clone https://github.com/hvribeiro/chvr.git
cd chvr/sources
mkdir bin
make
cd ../chvr/
pip install .
```

### 12. Enable Helvetica in Matplotlib. 

Verify the Matplotlib configuration file path
```{r, engine='bash', count_lines}
python3 -c "import matplotlib; print(matplotlib.matplotlib_fname())"
```

and update the path `~/clab/lib/python3.12/site-packages/` in the code below accordingly. 

You may also need to remove the font cache files:
```{r, engine='bash', count_lines}
rm ~/.matplotlib/fontList.cache ~/.matplotlib/fontManager.cache ~/.matplotlib/ttffont.cache
```

Extract Helvetica fonts:

```{r, engine='bash', count_lines}
cd ~/pypack/pyhvr/
tar -xvf helv.tar.gz -C ~/clab/lib/python3.12/site-packages/matplotlib/mpl-data/fonts/ttf/
mkdir ~/.matplotlib
cp ~/clab/lib/python3.12/site-packages/matplotlib/mpl-data/matplotlibrc ~/.matplotlib/.
```

Edit `~/.matplotlib/matplotlibrc` file and update the line starting with `'#font.sans-serif: DejaVu Serif, [...]'` to include Helvetica as the first option:
```{r, engine='bash', count_lines}
font.sans-serif: Helvetica, DejaVu Sans, [...]
```

Add `~/.matplotlib` to your PATH by adding the following line to the end of the `~/.bashrc` file.
```{r, engine='bash', count_lines}
export MPLCONFIGDIR=$HOME/.matplotlib
```


### 13.  Install NVIDIA driver version 535.

```{r, engine='bash', count_lines}
sudo apt install nvidia-driver-535
sudo reboot
```

### 14. Install the CUDA toolkit and cuDNN.

```{r, engine='bash', count_lines}
sudo apt install nvidia-cuda-toolkit nvidia-cudnn
```

### 15. Install TensorFlow and Torch with GPU support via `pip`.

```{r, engine='bash', count_lines}
python3 -m pip install tensorflow[and-cuda]
pip install --upgrade torch torchvision torchaudio
```

Test the installation:
```{r, engine='bash', count_lines}
python3 -c "from tensorflow.python.client import device_lib; print(device_lib.list_local_devices())"
python3 -c "import torch; print(torch.cuda.is_available())"
```

### 16. Install UMAP.

```{r, engine='bash', count_lines}
pip install umap-learn
```

### 17. Install PyMC with GPU support (JAX).

```{r, engine='bash', count_lines}
pip install "jax[cuda12]"
pip install pymc
pip install numpyro
```

### 18. Install DGL.

```{r, engine='bash', count_lines}
pip install dgl -f https://data.dgl.ai/wheels/torch-2.1/cu121/repo.html
```

### 19. Install `torch-geometric`.

First, install dependencies:
```{r, engine='bash', count_lines}
pip install torch-scatter torch-sparse torch-cluster
```

Then, install torch-geometric.
```{r, engine='bash', count_lines}
pip install torch-geometric
```